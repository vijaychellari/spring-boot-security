package com.wipro.springsecurity.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.wipro.springsecurity.model.User;
import com.wipro.springsecurity.web.dto.UserRegistrationDto;

public interface UserService extends UserDetailsService{
	User save(UserRegistrationDto registrationDto);
}
